#!/bin/bash -x

set -o nounset  # Fail when access undefined variable
set -o errexit  # Exit when a command fails


_opt_debug=false;
_opt_leave=false;
_opt_extract_rars=true;
query="";
#cmdl=`getopt -o l:s:dq: --long limit:,start:,debug,query: -- "$@"`
cmdl=`getopt -o dl --long debug,leave,norar -- "$@"`
eval set -- "$cmdl"
while true ; do
    case "$1" in
		-d|--debug)
			_opt_debug=true; shift;;
		-l|--leave)
			_opt_leave=true; shift;;
		--norar)
			_opt_extract_rars=false; shift;;
		--)
			shift
			[[ -z "$query" && ! -z ${1:-} ]] && query=$1;
			break;;
	esac
done

( $_opt_debug ) && echo -ne "Options debug:${_opt_debug} leave:${_opt_leave} query:$query\n";

TARFILE="${query}";

if [[ ! -e "${TARFILE}" ]]; then
	echo "Could not locate tar file. Usage: tarpx <tarfile>"
	exit 1
fi;

RAR_FILES_TO_PROCESS=();

OFS="$IFS"
IFS=$'\n';

for l in $(tar --list --file "${TARFILE}" | grep -v '/$' | tac); do
	# Check directory depth for strip-components
	lentotal=$(echo "${l}" | wc -c);
	lennoslash=$(echo "${l}" | tr -d "/" | wc -c);
	dirdepth=$(( $lentotal - $lennoslash ));
	stripcomponents="";
	[ $dirdepth -gt 0 ] && stripcomponents="--strip-components=${dirdepth}";

	suffix=$(echo $l | rev | cut -d '.' -f1 | rev);
	( $_opt_debug ) && echo "Found ${suffix,,} type file"

	if [[ "${suffix,,}" == "rar" ]]; then
		RAR_FILES_TO_PROCESS+=("$l");
	fi


	tar --file "${TARFILE}" $stripcomponents --extract "${l}";
	if [[ "$?" != "0" ]]; then
		echo "Error in extraction"; 
		exit 1;
	fi

	if [[ $_opt_leave == false ]]; then 
		tar --file "${TARFILE}" --delete "${l}";
		if [[ "$?" != "0" ]]; then
			echo "Error in removing from archive";
			exit 1;
		fi
	fi
	echo $l
	sync 
	#exit 0
	#echo $tarcmd;
done
IFS="$OFS"
sync



echo "Now do processing for ${RAR_FILES_TO_PROCESS}"