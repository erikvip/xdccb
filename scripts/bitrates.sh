bitrates() { for i in *.m??; do br=$(ffprobe "$i" 2>&1 | grep bitrate); echo -e "$i:\n\t$br"; done }

#ffinfo() { for f in *.m??; 

echoerr() { echo "$@" 1>&2; }

ffcheck() {
 	local _files _errors _errcount _fcount f;
	if [[ -z "$@" ]]; then
		_files=$(find . -maxdepth 1 -type f -iregex '^.*[maw][opvke][4vib]m?' -print | sort);
		_fcount=${#_fcount[*]};
	else
		_files=$*;
		if [[ ! -s "${_files}" ]]; then
			echo "Error: file not found ${_files}";
			return 1;
		fi
		_fcount=1;
	fi
	for f in $_files;
	do
		#echo "Checking last 60 seconds of ${_f}" > /dev/stderr;
		_errors=$(ffmpeg -v error -sseof -60 -i "${f}" -f null -threads 0 - 2>&1);
		_errcount=$(echo "${_errors}" | wc -l);
		(( _errcount-- ));
		#echo $_errors | sort | uniq;
		if [[ "${_errcount}" == 0 ]]; then
			echo "OK	${f}" > /dev/stderr;
			#echo -n $(true);
		else
			echo "Invalid	${f}" > /dev/stderr
			#echo -n $(false);
		fi
	done

}


ffinfo() {
	local f v a s
#		.format as \$f | .streams[0] as \$v | .streams[1] as \$a | .streams[2] as \$s | "\($f.probe_score)%\t\($f.filename|.[0:35])\t\($v.codec_name)\t\($v.width)x\($v.height)\t\($a.codec_name)\t\($a.channels)ch\tS#\($f.nb_streams)\t\($f.duration|tonumber/60/60|floor)hr\($f.duration|tonumber/60|floor%60)m\t\($f.size|tonumber/1024000|floor)MB\t\($f.bit_rate|tonumber/1024|floor)kbps"

	_tpl=$(cat <<-"EOT"
		def rpad(s; len; chr): s + chr * (len - (s|length));
		def lpad(s; len; chr): chr * (len - (s|length)) + s;
		def lpad(s; len): lpad(s;  len; "-");
		def rpad(s; len):rpad(s; len; " ");

	#	. | [.format.duration] as $wtf;
		. | .format as $f | .streams[0] as $v | .streams[1] as $a | .streams[2] as $s |
		$f.duration|tonumber/60/60|floor as $dur_hrs |
		$f.duration|tonumber/60|floor as $total_min |
		$f.duration|tonumber/60|floor%60|tostring as $dur_min |
		rpad($f.duration|tonumber|floor%60|tostring; 3) as $dur_sec |

		"\($f.probe_score)%"
		+"\t" + rpad($f.filename|.[0:38]; 38)
		+"\t" + rpad($v.codec_name; 6)
		+"\t" + rpad( "\($v.width)x\($v.height)" ; 10 )
		+"\t\($a.codec_name)"
		+"\t\($a.channels)ch"
		+"\tS#\($f.nb_streams)"
		+"\t" + ((if $f.duration|tonumber < (60 * 9)
			then
				"\($dur_min)m\($dur_sec)s" 
			else 
				"\($dur_hrs)hr\($dur_min)m" 
				#"\($dur_hrs):\($dur_min):\($dur_sec)"
			end))
		+"\t\($f.size|tonumber/1024000|floor)MB"
		+"\t\($f.bit_rate|tonumber/1024|floor)kbps"
		#+"\($wtf[])"
EOT
);
#	echo $_tpl;
#	#+"\t\($f.duration|tonumber/60/60|floor)hr\($f.duration|tonumber/60|floor%60)m"
	IFS=$'\n' 
	if [[ -z "$@" ]]; then
		_files=$(find . -maxdepth 1 -type f -iregex '^.*[maw][opvke][4vib]m?' -print | sort);
	else
		_files=$*;
	fi
	for f in $_files;
	do 
#		echo "f=$f" > /dev/stderr
#		r=0;

		#j="$(ffprobe -hide_banner -of json -show_entries "stream=index,codec_name,codec_type,pix_fmt,width,height,sample_rate,channels : format=filename,nb_streams,format_name,duration,size,bit_rate,probe_score" -loglevel 0 "$f" || r=$?)";
#		jq -r "${_tpl}" <(ffprobe -hide_banner -of json -show_entries "stream=index,codec_name,codec_type,pix_fmt,width,height,sample_rate,channels : format=filename,nb_streams,format_name,duration,size,bit_rate,probe_score" -loglevel 0 "$f");
#		read _json  <(ffprobe -hide_banner -of json -show_entries "stream=index,codec_name,codec_type,pix_fmt,width,height,sample_rate,channels : format=filename,nb_streams,format_name,duration,size,bit_rate,probe_score" -loglevel 0 "$f");
#		set +o verbose
#		set +o xtrace
#		coproc ffp ( ffprobe -hide_banner -of json -show_entries "stream=index,codec_name,codec_type,pix_fmt,width,height,sample_rate,channels : format=filename,nb_streams,format_name,duration,size,bit_rate,probe_score" -loglevel 0 "$f" 2>/dev/null ) 2> /dev/null >/dev/null  
#		{${ffp[1]}}>&-  
#		j=$(cat -v <&${ffp[0]});
		#j="$(cat <(${ffp[0]}) )";
#		echo $j;
#		echo "ffp_PID=$ffp_PID"
#		wait $ffp_PID  2> /dev/null > /dev/null
#		echo "Return: $?"


		ffprobe -hide_banner -of json -show_entries "stream=index,codec_name,codec_type,pix_fmt,width,height,sample_rate,channels : format=filename,nb_streams,format_name,duration,size,bit_rate,probe_score" -loglevel 0 "$f" \
		 | jq -r "${_tpl}";
	done


#	done | jq -r "${_tpl}" | column -t -s $'\t' #| sort -k 9 -t $'\t' | sort -k 10 -g



#		'.format as $f | .streams[0] as $v | .streams[1] as $a | .streams[2] as $s | "\($f.probe_score)%\t\($f.filename|.[0:35])\t\($v.codec_name)\t\($v.width)x\($v.height)\t\($a.codec_name)\t\($a.channels)ch\tS#\($f.nb_streams)\t\($f.duration|tonumber/60/60|floor)hr\($f.duration|tonumber/60|floor%60)m\t\($f.size|tonumber/1024000|floor)MB\t\($f.bit_rate|tonumber/1024|floor)kbps"' \

# '.format as $f | .streams[0] as $v | .streams[1] as $a | .streams[2] as $s | "\($f.probe_score)%\t\($f.filename|.[0:25])\t\($v.codec_name)\t\($v.width)x\($v.height)\t\($v.pix_fmt)\t\($a.codec_name)\t\($a.channels)ch\tS#\($f.nb_streams)\t\($f.duration|tonumber/60/60|floor)hr\($f.duration|tonumber/60|floor%60)m\t\($f.size|tonumber/1024000|floor)MB\t\($f.bit_rate|tonumber/1024|floor)kbps"' \

}

export -f ffinfo



