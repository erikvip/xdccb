#!/bin/bash
set -o nounset; # Fail when accessing an undefined variable
set -o errexit; # Exit if command fails

## @TODO Single quote escaping in the remote SSH command is fucking retarded and bandaid fixes with double quotes...a double quote in the filename will still break it

## Default 
DL_URL='https://jap.0x.no/irc/irc/done/';
DL_REMOTE_PATH="irc/done/";
DL_REMOTE_PATH_FINISHED="irc/done/fin/";



#DL_URL='https://jap.0x.no/irc/irc/donert/';
#DL_REMOTE_PATH="irc/donert/";
#DL_REMOTE_PATH_FINISHED="irc/donert/fin/";




# Vagabon
#DL_URL='https://jap.0x.no/irc/irc/done/savert/krcl/';
#DL_REMOTE_PATH="ircrt/done/save/krcl/";
#DL_REMOTE_PATH_FINISHED="ircrt/done/fin/";

# krcl afternoon
#DL_URL='https://jap.0x.no/irc/irc/krcl/';
#DL_REMOTE_PATH="irc/krcl/";
#DL_REMOTE_PATH_FINISHED="irc/done/fin/";


DL_SSH_HOST="erikp@jap.0x.no";
DL_HISTORY="finished.dat";

if [ ! -e "${DL_HISTORY}" ]; then
	touch "${DL_HISTORY}";
fi

_opt_shuffle=false;
_opt_interactive=false;
_opt_debug=false;
_opt_list=false;
_opt_wait=""; 
query="";
#cmdl=`getopt -o l:s:dq: --long limit:,start:,debug,query: -- "$@"`
cmdl=`getopt -o dsilw: --long list,debug,shuffle,interactive,wait: -- "$@"`
eval set -- "$cmdl"
while true ; do
    case "$1" in
    	-s|--shuffle)
			_opt_shuffle=true; shift;;
		-i|--interactive)
			_opt_interactive=true; shift;;
		-d|--debug)
			_opt_debug=true; shift;;
		-l|--list)
			_opt_list=true; shift;;
		-w|--wait)
			_opt_wait=$2; shift; shift;;
		--)
			shift
			[[ -z "$query" && ! -z ${1:-} ]] && query=$1;
			break;;
	esac
done

( $_opt_debug ) && echo -ne "Options shuffle:${_opt_shuffle} interactive:${_opt_interactive} debug:${_opt_debug} wait:${_opt_wait} query:$query\n";

# Remove plus sign + to space transform
#_urldecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }
_urldecode() { echo -e "${*//%/\\x}"; }

# Fetch the apache 'Index of /' and return a URL for each item, skipping table header links
fetch_list() {
	echo "Fetching file list..." > /dev/stderr

	( $_opt_shuffle ) && cmd="shuf" || cmd="sort";
	( $_opt_debug ) && wgetopt="-v" || wgetopt="-q"; 
	( $_opt_interactive ) && choose="fzf --multi --sync --bind tab:toggle" || choose="cat";
	#wget -q -O - "${DL_URL}" \
	#curl "${DL_URL}" \

	wget $wgetopt -O - "${DL_URL}" \
		| htmlq -S "" -s "\n" -a href 'body table tr td[valign] + td a' \
		| grep -v '/$' \
		| sed "s#^#${DL_URL}#g" \
		| $cmd \
		| $choose;

#	echo wget -q -O - 'https://jap.0x.no/irc/irc/done/' | \
#		grep '<tr><td' | \
#		grep -v 'folder.gif' | \
#		egrep -o 'href="[^"]*' | \
#		cut -d'"' -f2 | \
#		grep -v '/irc/' | \
#		sed 's#^#https://jap.0x.no/irc/irc/done/#g' | \
#		sort #| shuf
}

#query=${1:-};

main () {
	if ( $_opt_list ); then fetch_list; exit; fi

	for u in $( fetch_list ); do 
		file=$(basename "$u");
#		file="${file//\'/\\\'}"
		if [[ "$query" != "" ]]; then

			if [[ "${query:0:1}" == "-" ]]; then
				q="${query:1}";

				# Query is negated. Match files not containing query
				if [[ "${u^^}" =~ "${q^^}" ]]; then
					echo "${bold}Skipping${normal} $file as it matches ${underline}$query${end_underline}";					
					continue;
				fi
				echo "Negative match on $u";
			else
				if [[ ! "${u^^}" =~ "${query^^}" ]]; then
					echo "${bold}Skipping${normal} $file as it does not match ${underline}$query${end_underline}";
					continue;
				fi
				echo "Match on $u"; 
			fi
		fi

		if [[ `grep -c "$file" finished.dat` != 0 ]]; then
			echo "${bold}Skipping${normal} $u as its already in the finished.dat file";
			continue;
		fi

		if [[ ! -z "$_opt_wait" ]]; then
			echo "Sleeping for $_opt_wait ..."; 
			sleep $_opt_wait
			if [ $? -ne 0 ]; then
				echo "ERROR: Invalid wait timeout specified for 'sleep'."
				exit 1;
			else
				_opt_wait="";
			fi
		fi


		echo "${bold}Downloading $u${normal}"; echo;
		aria2c --dscp=32 -c "$u"; 
		r=$?;
		if [[ "$r" == 0 ]]; then
			# Download success
			file=$(_urldecode "${file}");
			echo "Download finished for ${file}. Verifying checksums...";
			remote_cksum=$(ssh "${DL_SSH_HOST}" "cksum \"${DL_REMOTE_PATH}${file}\"" | cut -d " " -f1,2 | tr -d "\n");
			local_cksum=$(cksum "${file}" | cut -d " " -f1,2 | tr -d "\n");
#			echo "$file :: remote checksum: ${remote_cksum}  local checksum: ${local_cksum}";


			if [[ "${remote_cksum}" != "${local_cksum}" ]]; then
				echo; echo "${red}ERROR: Checksums do not match for $u${normal}"; echo
				exit 1;
			fi
			echo "${bold}Success. Moving remote ${file} to ${DL_REMOTE_PATH_FINISHED}";

#			ssh ${DL_SSH_HOST} "mv '${DL_REMOTE_PATH}${file}' ${DL_REMOTE_PATH_FINISHED}";
			ssh ${DL_SSH_HOST} "mv \"${DL_REMOTE_PATH}${file}\" ${DL_REMOTE_PATH_FINISHED}";

			echo "Return $?"
			echo -e "${u}\t${remote_cksum}" >> finished.dat

		fi;
	
	done
}




# Color helpers
normal=$(tput sgr0);

# Standard color set
black=$(tput setaf 0);
red=$(tput setaf 1);
green=$(tput setaf 2);
yellow=$(tput setaf 3);
blue=$(tput setaf 4);
magenta=$(tput setaf 5);
cyan=$(tput setaf 6);
white=$(tput setaf 7);

# Extended ANSI colors
light_red=$(echo -e "\033[1;31m")
light_green=$(echo -e "\033[1;32m")
light_yellow=$(echo -e "\033[1;33m")
light_blue=$(echo -e "\033[1;34m")
light_magenta=$(echo -e "\033[1;35m")
light_cyan=$(echo -e "\033[1;36m")

# Text effects
bold=$(tput bold);
underline=$(tput smul);
end_underline=$(tput rmul);
blink=$(tput blink);
standout=$(tput smso);
end_standout=$(tput rmso);

# Cursor Positioning
#cursor_up="\033[1A";
cursor_up=$(tput cuu1);
cursor_down=$(tput cud1);


main

