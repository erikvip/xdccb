BEGIN { 
    # Only output header if skipheader is not set to 1
    #if ( SKIPHEADER != 1 )
    #    print "'Network', 'Channel', 'Nick', 'Pack #', 'DLs', 'Size', 'Type', 'File', 'Cmd'" 
}
{
    # 1 - nick
    # 2 - number
    # 3 - requests
    # 4 - size
    # 5 - file
    FILE=substr($0, index($0,$5))
    TITLE=FILE
    ITEMID=""
    HINT="unknown"
    YEAR=""
    IGNORECASE = 1

    if ( match(FILE, /\.XXX\./) != 0 )
        HINT="XXX"
    if ( match(FILE, /[\.\-](EBOOK|E\-BOOK|EPUB|PDF|BOOKWARE)[\.\-]/) != 0 )
        HINT="eBook"
    if ( match(FILE, /[\.\-](X86|X64|APK|Patch|Update|Android|ISO|Keygen|KeyMaker|Linux|MacOS|MacOSX[\.\-])/) != 0 )
        HINT="APP"

    if ( match(FILE, /[\.\-\[]+(MP3|VBR|[2-9]CD|CD|FLAC|Discography|OST|Vinyl)[\.\-\]]+/) || match($1, /(Music|MP3)/) || match(CHANNEL, /(Music|MP3)/) || match(FILE, /^\[?[^\]*]?\]?_?[^\.\-]+-[^\.\-]+-.*(zip|tar)$/) )
        HINT="Sound"

    if ( match($1, /Anime/) )
        HINT="TV/Anime"

    # If we still have an Sxx (Season) tag at this point, assume it's a TV series, parse year and title
    if ( match(FILE, /\.S[0-9]{2}\./) || match(FILE, /\.S[0-9]{2}E[0-9]{2}\./) ) {
        HINT="TV"
        TITLE=substr(TITLE,0,RSTART-1)
        ITEMID=substr(FILE,RSTART+1, RLENGTH-2)
    }

    if ( HINT == "unknown" ) {
        if ( match(FILE, /(\.|-|_)(HEVC|2160p|BluRay|HDRIP|BDRIP|BRrip|DVDrip|DVDScr|XVID|HDCAM|HDTS|HD\-TS)(\.|-)?/) || match(FILE, /(\.|-)(h|x)\.?26(4|5)(\.|-)?/) || match(FILE, /\.(mkv|mp4|avi|mpg)$/) || match($1, /(Movies|Sequel|Animated)/) ) {
            HINT="Movies"
            TITLE=substr(TITLE,0,RSTART-1)
        }

        # Last try, try to assign using their nickname / category. 
        else if ( match($1, /[\-\|]APP[\-\|]/) )
            HINT="APP"
        else if ( match($1, /[\-\|]MUSIC[\-\|]/) )
            HINT="Sound"

        # Check for TV matches in nickname and set assume hint is TV
        if ( match($1, /[\-\|](TV|HDTV)[\-\|]/ ) && HINT!="Movies" ) {
            HINT="TV"
        }


        # 'Movies' with a date tag should probably be TV...
        #if ( HINT == "MOVIES" )
    }
    if ( match(FILE, /\.(19|20)[0-9]{2}\.[0-9]{2}\.[0-9]{2}\./) ) {
        TITLE=substr(TITLE,0,RSTART-1)
        ITEMID=substr(FILE,RSTART+1, RLENGTH-2)
        if ( HINT == "unknown" )
            HINT="TV"
    }

    if ( match(FILE, /(\.|-)(19|20)[0-9]{2}(\.|-)/, y) ) {
        YEAR=substr(FILE, RSTART+1, RLENGTH-2)
        TITLE=substr(TITLE,0,RSTART-1)
        #print substr(FILE,0)
    }

    gsub(/\./, " ", TITLE)
    
    #csv="'"NETWORK"', '"CHANNEL"', '"$1"', '"$2"', '"$3"', '"$4"', '"HINT"', '"FILE"',/MSG "$1" XDCC SEND #"$2""
    URI=NETWORK"/"CHANNEL"/"$1"/"$2
    URL=URI"/"FILE

    csv="'"NETWORK"','"CHANNEL"','"$1"','"$2"','"$3"','"$4"','"HINT"','"FILE"','"TITLE"','"YEAR"','"ITEMID"','"URI"','"URL"','',''"
    count++

    
    #print count
    print csv
    
}
