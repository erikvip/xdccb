
CREATE TEMPORARY TABLE import LIKE packs;

LOAD DATA LOCAL INFILE 'listing-20220122194741.csv' INTO TABLE import FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY "'" LINES TERMINATED BY '\n';

//Existing, unchanged rows
select count(*) from import join packs using (uri, url);

INSERT INTO packs ( SELECT i.network, i.channel, i.nickname, i.packno, i.downloads, i.size, i.type, i.filename, i.title, i.year, i.itemid, i.uri, i.url, NOW(), p.lastupdate FROM import i LEFT JOIN packs p USING (uri, url) WHERE i.uri=p.uri and i.url=p.url and p.url IS NOT NULL) ON DUPLICATE KEY UPDATE downloads=i.downloads,lastannounced=NOW();

//Updated rows
select count(*) from import i join packs p using (uri) where i.filename!=p.filename;

REPLACE INTO packs ( SELECT i.network, i.channel, i.nickname, i.packno, i.downloads, i.size, i.type, i.filename, i.title, i.year, i.itemid, i.uri, i.url, NOW(), NOW() FROM import i LEFT JOIN packs p USING (uri) WHERE i.uri=p.uri AND i.filename!=p.filename and p.filename IS NOT NULL)

//Newly created rows
select count(*) from import where uri not in ( select uri from packs );

INSERT INTO packs ( SELECT i.network, i.channel, i.nickname, i.packno, i.downloads, i.size, i.type, i.filename, i.title, i.year, i.itemid, i.uri, i.url, NOW(), NOW() FROM import i WHERE i.uri NOT IN ( SELECT uri FROM packs) ); 





