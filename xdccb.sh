#!/bin/bash
#declare -A xdccb=( [CONFIG_FILE]="${HOME}/.xdccb.conf" [DEBUG]=false )
BASEDIR=$(dirname `realpath $0`);
source "${BASEDIR}/web/.env";

_start=0; 
_limit=100;
_debug=0;
_query="";
cmdl=`getopt -o l:s:dq: --long limit:,start:,debug,query: -- "$@"`
eval set -- "$cmdl"
while true ; do
    case "$1" in
    	-l|--limit)
			_limit=$2;
			shift 2;	
			;;
		-s|--start)
			_start=$2;
			shift 2;
			;;
		-q|--query)
			_query=$2;
			shift 2;
			;;
		-d|--debug)
			_debug=1;
			shift;
			;;
		--) 
			shift
			if [ -z "$_query" ]; then
				_query=$*; 
			fi
			break 
			;;
	esac
done

debug() {
    local _msg="$@";
    if [ "$_debug" == "1" ]; then echo "$_msg"; fi
}

debug "Command line: start:${_start} limit:${_limit} debug:$_debug} query:${_query}";

#while getopts "s:l:" OPTKEY; do
#	[ "${OPTKEY}" == "s" ] && _start="$OPTARG";
#	[ "${OPTKEY}" == "l" ] && _limit="$OPTARG";

#done
	
xdccb_mysql_conf=$(mktemp);

cleanup() {
	rm -f "${xdccb_mysql_conf}";
}
trap cleanup EXIT


if [[ ! -z "${TERM}" && "${TERM}" != "dumb" ]]; then
	normal=$(tput sgr0);
	red=$(tput setaf 1);
	green=$(tput setaf 2);
	yellow=$(tput setaf 3);
	bold=$(tput bold);
	underline=$(tput smul);
fi
# Make temporary mysql login file from .env
echo "[client]
user=${xdccb_mysql_user}
password=${xdccb_mysql_password}
host=${xdccb_mysql_host}
database=${xdccb_mysql_database}
port=${xdccb_mysql_port}
" > "${xdccb_mysql_conf}";


for line in $(echo "$xdccb_watch_logfiles" | tr "\r\n\t" "   " | sed -r 's/  +/ /g'); do 
	_network=$(echo "${line}" | cut -d '/' -f1);
	_channel=$(echo "${line}" | cut -d '/' -f2);
	_file=$(echo "${line}" | cut -d '/' -f3-);
	_file=${_file//\~/$HOME};
#	_itemconf=( [NETWORK]="${_network}" [CHANNEL]="${_channel}" [FILE]="${_file}" );
done

debug() {
	echo "$*" > /dev/stderr
}

mysql_query() {
	_sql="$*";
	#echo $_sql;exit;
	for v in "${BASH_ARGV[@]@Q}"; do
		_cmdline="${_cmdline} $(echo ${v} | rev)"
	done
	_cmdline=$(echo ${_cmdline} | rev);
	#_cmdline="${BASH_ARGV[@]@Q}";
	#echo $_cmdline | rev;exit;
	_prefix_cmd="";
	if [[ "${HOSTNAME}" == "${remote_ssh_when_localhost}" && "${remote_ssh_when_localhost}" != "" ]]; then
		echo "${bold}${red}Querying remote ssh host ${normal}${underline}${remote_ssh_hostname}${normal}" > /dev/stderr
		debug "ssh ${remote} xdccb ${_cmdline}";
		ssh ${remote_ssh_hostname} "xdccb ${_cmdline}" | pv -rabetp -s $(( $_limit * 100 ))
	else
		if [[ "${_debug}" == 1 ]]; then
			echo "${bold}${yellow}${_sql}${normal}" > /dev/stderr
		fi
		echo "${_sql}" | mysql --defaults-extra-file="${xdccb_mysql_conf}" --database="${xdccb_mysql_database}"
	fi
}

main() {
	run_query "$@"
}

#######################################
# Build SQL Query with advanced search operators
# Using 'finite state machine' ...!
#
# Operators: 
#	+: logical AND (implied) or the word ' AND '
#	-: logical NOT. Negate query (like: +contain -notcontained) or ! or the word NOT
#	,: logical OR (vinyl, mp3) Or the word ' OR '
#	(): Evaulation order
#	@: Network
#	#: Channel
#	$: Nickname
#	\: Quote next character
#	[: Specify type
#	]: Specify NOT type
#	%: Wildcard (sql default)
#	*: Wildcard (converted to % for sql)
#	~: regex search
#	>: GREATER THAN timespec
#	<: LESS THAN timespec
#	: shorthand for search in results
#######################################
build_query() {
	local current_state="BEGIN";
	local current_pos=0;
	local current_char;
	local last_char;
	local last_state;
	local r_control_chars="\+\-\,\(\)\@\#\/\\\^\%\*\~\>\<\:"
	local op="LIKE";
	local term="url";
	local out="";
	local where=();
	local types=();
	local keyword;

debug ${_query}
	for ((i=0 ; i<${#_query} ; i++ )); do
		current_pos=i;
		current_char=${_query:i:1};
		if [[ "${current_char}" =~ [${r_control_chars}] ]]; then
			#current_state="CONTROL";
			debug "CONTROL CHARACTER: $current_char";
			case "${current_char}" in
				"%"|"*")
					q+="%";
					current_state="KEYWORD";
					;;
				"^")
					current_state="TYPE";
					;;
			esac
		elif [[ "${current_char}" == " " ]]; then
			if [[ "${current_state}" == "TYPE" ]]; then
				types[]="${keyword}"
				keyword="";
			fi	
			current_state="SPACED";
			q+="%' )";
		elif [[ "${current_state}" == "TYPE" ]]; then
			keyword
		else
			out+="${current_char}";
			if [[ "${current_state}" != "KEYWORD" ]]; then
				if [[ "${current_state}" != "BEGIN" ]]; then 
					q+=" AND ";
				fi
				q+="( ${term} ${op} '%";
			fi
			current_state="KEYWORD";
			q+="${current_char}";
		fi
		last_state="${current_state}";
		last_char="${current_char}";
	done
	q+="%')";

	debug "$q"
	echo "$q"
}

#build_filter_types() {
	
#}

_validate_datespec() {
	_check=$(date --date="$1" 2>&1);
	r=$?;
	if [[ $r != 0 ]]; then
		echo -e "ERROR: invalid datespec ${1}\n${_check}\n" > /dev/stderr;
		exit 1;
	fi
}
_get_unixtime() {
	_validate_datespec "$1";
	echo $(date --date="$1" "+%s");
}

#if [[ "$1" =~ ^[^\/]*\/\#[^\/]*\/[^\/]*\/[0-9]+\/.*$ ]]; then
run_query() {
	local q qarr keywords f_type f_not_type f_time_gt f_time_lt;
	if [[ "$_query" =~ ^[^\/]*\/\#[^\/]*\/[^\/]*\/[0-9]+\/.*$ ]]; then
		url="$_query";
		#echo "INSERT INTO msg (msgdata) VALUES ('${url}');" \
		#	| mysql --user="${xdccb[MYSQL_USER]}" --password=${xdccb[MYSQL_PASS]} --database=${xdccb[MYSQL_DB]}
		mysql_query "INSERT INTO msg (msgdata) VALUES ('${url}');";
	else
		q="$_query";
		qarr=( "$@" );
		#echo $@; exit;
		#Enable case insensitive string replacement
		shopt -s nocasematch

		# Sanitize - remove quotes
		q=${q//\'/};
		q=${q//\"/};

		# Extract filters
		#IFS=$' ';
		for i in "${qarr[@]}"; do
			debug "i=${i}";
			case "$i" in
				\[*) 
					[ ! -z "${f_type}" ] && f_type="${f_type},${i:1}";
					[ -z "${f_type}" ] && f_type="${i:1}";
					shift;;
				\]*) 
					[ ! -z "${f_not_type}" ] && f_not_type="${f_not_type},${i:1}";
					[ -z "${f_not_type}" ] && f_not_type="${i:1}";
					shift;;
				\>*)
					f_time_gt=$(_get_unixtime "${i:1}");
					shift;;
				\<*)
					f_time_lt=$(_get_unixtime "${i:1}");
					shift;;
				*)
					keywords="${keywords} ${i}";
					shift;;
			esac
		done

		q="${keywords}";

		debug "Filters: type=${f_type}";
		debug "Filters: time greater than=${f_time_gt}";
		debug "Filters: time less than=${f_time_lt}";
		debug "Keywords: ${keywords}";

		# tokenize special search operators
		q="${q// AND /&&}"
		q="${q// OR /||}"
		q="${q// NOT /!}"
		#q="${q// REGEXP /~}"	
		#q="${q// ~ /~}"	

		# Wildcard handling
		q="${q// /%}";
		q="${q//\?/_}";

		# Handle advanced search operators
		q="${q//&&/%\') AND ( url LIKE \'%}";
		q="${q//||/%\') OR ( url LIKE \'%}";
		q="${q//!/%\') AND ( url NOT LIKE \'%}";


		# Build final query...
		# TODO: Still doens't work if we begin the whole thing with NOT
		q="( url LIKE '%${q}%' )"
		
		# Filters: type
		[ ! -z "${f_type}" ] && q="${q} AND type IN ('${f_type//,/\',\'}') ";
		[ ! -z "${f_not_type}" ] && q="${q} AND type NOT IN ('${f_not_type//,/\',\'}') ";

		# Filters lastupdate
		[ ! -z "${f_time_gt}" ] && q="${q} AND lastupdate >= FROM_UNIXTIME('${f_time_gt}')";
		[ ! -z "${f_time_lt}" ] && q="${q} AND lastupdate =< FROM_UNIXTIME('${f_time_lt}')";

echo $q;exit;
		_sql_fields="size,url";
		#_sql_base="SELECT :fields FROM packs WHERE url LIKE '%${q}%' ORDER BY lastupdate DESC";
		_sql_base="SELECT :fields FROM packs WHERE ${q} ORDER BY lastupdate DESC";
		_sql_limit="LIMIT ${_limit} OFFSET ${_start}";
		mysql_query "${_sql_base/:fields/$_sql_fields} ${_sql_limit}; ${_sql_base/:fields/count(*) AS TOTAL}";

		#mysql_query "SELECT SQL_CALC_FOUND_ROWS size,url FROM packs WHERE url LIKE '%${q}%' ORDER BY lastupdate DESC LIMIT ${_limit} OFFSET ${_start};SELECT CONCAT('TOTAL: ',FOUND_ROWS()) AS '';";
		#mysql_query "SELECT CONCAT( Type, ':', size, ':', downloads, 'x:',  ROUND(TIMESTAMPDIFF(MINUTE,lastannounced,NOW())/60,2),"hrs",url) FROM packs WHERE url LIKE '%${q}%' ORDER BY lastupdate DESC LIMIT 200";
		
	#	echo "SELECT size,url FROM packs WHERE url LIKE '%${q}%' " \
	#		| mysql --user="${xdccb[MYSQL_USER]}" --password="${xdccb[MYSQL_PASS]}" --database="${xdccb[MYSQL_DB]}"
	fi
}



main "$@"
