 #!/bin/bash -x

LOGFILE=${1:?usage $0 <path to logfile> <network name> <channel name - NO '#'>};
NETWORK=${2:?usage $0 <path to logfile> <network name> <channel name - NO '#'>};
CHANNEL=${3:?usage $0 <path to logfile> <network name> <channel name - NO '#'>};
#SKIPHEADER=${4:-0};

#(>&2 echo "Reading input from stdin (you must send the file(s) to stdin...)")

BASEDIR=$(dirname $0);

#    | grep -vi '.German.' \
 #   | grep -vi '.French.' \

cat "$LOGFILE" \
	| tr -dc "_A-Z-a-z-0-9 :#_\-\.\n\[\]\|" \
	| cut -d ' ' -f2- \
	| egrep -v ' has (joined|left|quit)' \
	| sed -e "s/^ //g" \
	| egrep '^[^ ]* \#[0-9]+' \
	| sed -r \
		-e 's/\[([ 0-9\.]+[MGKmgk]{1})\]/\1/g' \
		-e 's/ \#([0-9]*) / \1 /g' \
		-e 's/ +([0-9]+)x/ \1/g' \
	| sort | uniq \
	| awk \
		-v NETWORK="${NETWORK}" \
		-v CHANNEL="${CHANNEL}" \
		-v SKIPHEADER="${SKIPHEADER}" \
		-F ' ' \
		-f "${BASEDIR}/import.awk" \
	| sort -t "," -k8 | sort -t "," -k7		
