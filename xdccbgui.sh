#!/bin/bash 
set -o nounset  # Fail when access undefined variable
set -o errexit  # Exit when a command fails

_res=$(mktemp "${TMPDIR}/xdccbgui-res.XXXX");
#_res="/tmp/xdccbgui-res.doXs";
export _res
do_xdccb() {
	notify-send "$1"
#	echo $*;
#	exit;
	xdccb "$1"
	notify-send.sh -i start-here -a xdccbgui XDCCB "$1"
}
export -f do_xdccb

get_results() {
	_sort_opt=${1:--h};
	if [[ "$_sort_opt" == "filename" ]]; then
		_sort_opt="-k 2";
	elif [[ "$_sort_opt" == "size" ]]; then
		_sort_opt="-h";
	fi
#	| awk -F / '{ print $1"\t"substr($6,0,80)"\t"$5"\t"substr($4,0,25)"\t"$3"\t"$2 }' \
	cat $_res \
		| grep -vE '^size.*url$' \
		| tr "\t" "/" \
			| awk -F / '{ print $1"\t"$6"\t"$5"\t"$4"\t"$3"\t"$2}' \
		| sort $_sort_opt \
		| column -t -s $'\t' -o $'\t'

}
export -f get_results

do_preview() {
	base=$(echo "$1" | xargs basename); 
	#if [ `echo "$1" | grep "XXX"` ]; then
	if [[ "$base" =~ .*XXX.* ]]; then
		keyword=$(echo "${base}" | egrep -o '^.*XXX');
		rplkeyword=$(echo "${keyword}" | tr "." "-");
		url="https://duckduckgo.com/?q=${keyword}";
		url="https://pornrips.to/${rplkeyword,,}";
		google-chrome --incognito "${url}" 2> /dev/null > /dev/null & 

	elif [[ "${base}" =~ \[[^\]]*\]_[^-]*-[^-*] ]]; then
		artist=$(echo "$base" | cut -d '_' -f2- | cut -d '-' -f1 | tr "_" " ");
		album=$(echo "$base" | cut -d '_' -f2- | cut -d '-' -f2 | tr "_" " ");
		genre=$(echo "$base" | cut -d '_' -f1 | tr -d '][');
		echo "Artist: $artist album: $album genre:$genre";

		url="https://last.fm/music/${artist}/${album}"
		google-chrome "${url}" 2> /dev/null > /dev/null
	fi
	

}
export -f do_preview

main() {
	_query="$@"
	xdccb "$@" > $_res
	get_results -h \
		| fzf -i \
		--delimiter $'\t' \
		--no-sort \
		--exact \
		--preview-window=bottom,12%,nofollow,wrap \
		--preview-window=hidden \
		--preview "echo {6}/{5}/{4}/{3}/{2}" \
		--header "$_query" \
		--bind "alt-v:toggle-preview" \
		--bind "esc:abort" \
		--bind "alt-s:reload(get_results size)" \
		--bind "alt-f:reload(get_results filename)" \
		--bind "alt-p:execute(do_preview {6}/{5}/{4}/{3}/{2})" \
		--bind "alt-e:execute(dialog --inputbox Search: 8 40 2>/tmp/wtf)+reload(xdccb `cat /tmp/wtf` > $_res)" \
		--bind "enter:preview(do_xdccb {6}/{5}/{4}/{3}/{2})" 
		
#		--bind "enter:preview(echo {+} | tr ' ' / | sed 's#//*#/#g')"
	#	--bind "enter:execute(do_xdccb {+f} {+n})"
#		--bind "enter:execute(do_xdccb {6}/{5}/{4}/{3}/{2})" 
#		--bind "enter:execute(do_xdccb {+6}/{+5}/{+4})" 
		#--bind "enter:execute(do_xdccb {6}/{5}/{4}/{3}/{2})" 
	#--preview "echo {6}/{5}/{4}/{3}/{2}" \
}

cleanup() {
	rm "${_res}"
	:
}

trap cleanup EXIT


main $*
