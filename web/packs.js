var humanizeDuration = require("humanize-duration");

exports.html = function(req, res) {
	//var dt = require( 'datatables.net' );
	if ( req.app.locals.packs.length == 0 ) {
		res.send("0 packs found")
	}
	var rowStripe="odd";
	res.render('files', { 
		title: 'Recently Added / Updated', 
		results: req.app.locals.packs, 
		duration: function(ms) {
			return humanizeDuration(ms, {largest: 2, round: true})
		}
	})

};

exports.buildsql_where = function(req, res) {
	attr={
		"where":['filename IS NOT NULL'], 
		"order by":[{'key':'lastupdated', 'dir':'desc'}], 
	};
	var wherecols = ['network', 'channel', 'nickname', 'type'];

	for (const key in req.query) {
		v = req.query[key]; 
		if ( typeof v == 'string' ) v=[v];
		//Quote each item
		//for (const i in v) v[i]=res.app.mysql.escape(v[i]);

		if (wherecols.includes(key) ) attr['where'].push(key + " IN (" + res.app.mysql.escape(v) + ")");
	}
	if ( req.query['search'] != undefined ) {
		var q = req.query['search']['value'].replace("'", "");
		attr['where'].push("filename LIKE '%" + q + "%'");
	}

	var where = attr['where'].join(' AND ');
	return where;
}
exports.buildsql = function(req, res) {
	
	var where = exports.buildsql_where(req, res); 
	var limit = 100; 
	var offset = 0; 

	if ( parseInt(req.query['length']) && req.query['length'] < 1000 ) {
		limit = req.query['length'];
	}
	if ( parseInt(req.query['start']) ) {
		offset = parseInt(req.query['start']);
	}

	var _sql=`
		SELECT 
			type,
			network,
			channel, 
			nickname, 
			packno, 
			downloads, 
			size, 
			filename, 
			title, 
			year, 
			url, 
			lastupdate, 
			( UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(lastupdate) ) * 1000 AS lastupdate_ms_ago
		FROM
			packs
		WHERE 
			`+where+`
		ORDER BY 
			lastupdate DESC
		LIMIT ` + limit + ` OFFSET ` + offset + `
		`;
//		console.log(_sql);
	return _sql;
};
exports.buildsql_count = function(req, res) {
	var where = exports.buildsql_where(req, res); 
	var _sql=`
		SELECT 
			count(*) AS recordsFiltered,
			( SELECT COUNT(*) FROM packs ) AS recordsTotal
 		FROM
			packs
		WHERE 
			`+where+`
		`;
//		console.log(_sql);
	return _sql;

}

exports.load = function(req, res, next) {
	var _sql=exports.buildsql(req, res);
	var _sqlcount=exports.buildsql_count(req, res);
	
	//Fetch counts
	req.app.db.query(_sqlcount, function (error, results, fields) {
		if (error) {
			console.log(error);
			throw error;
		}
		req.app.locals.meta = results;
		req.app.db.query(_sql, function (error, results, fields) {
			if (error) {
				console.log(error);
				throw error;
			}
//			console.log(results[0]);
			req.app.locals.packs = results;
			next();
		});

	});		
};
exports.json = function(req, res, next) {
	exports.load(req, res, function() {
		var data = {
			draw : req.query['draw'],
			recordsTotal: req.app.locals.meta[0].recordsTotal,
			recordsFiltered: req.app.locals.meta[0].recordsFiltered,
			data: req.app.locals.packs
		};
		res.jsonp(data);
	});
//	console.log('send resp');
//	console.log(req.app.locals);
};