//echo -ne 'data:image/jpeg;charset=utf-8;base64,' `curl -s https://image.tmdb.org/t/p/w200/tuGlQkqLxnodDSk6mp5c2wvxUEd.jpg | convert - -resize 150x150 -quality 5 - | base64 | tr -d "\n" ` >o
var humanizeDuration = require("humanize-duration");
exports.html = function(req, res) {
	//console.log(req.app.locals.titles);
	if ( req.app.locals.titles.length == 0 ) {
		res.send("0 packs found")
	}
	res.render('browse', { 
		title: 'Browse', 
		results: req.app.locals.titles, 
		duration: function(v) {
			var dt = new Date(v);
			//var ms = dt.getTime();
			var msago = new Date().getTime() - dt.getTime();
			//console.log(ms);
			return humanizeDuration(msago, {largest: 2, round: true})
		},
		posterimg: function(uri) {
			return ( uri == null ) ? '/images/film_placeholder.jpg' : 'https://image.tmdb.org/t/p/w200/'  + uri;
		}, 
		rating_color: function(percent) {
			var cls="rating-color-" + parseInt(percent);
			return cls;
		},
		genre_name: function(genre_ids) {
			if ( genre_ids == null ) return '';
			const genres = [{"id":28,"name":"Action"},{"id":12,"name":"Adventure"},{"id":16,"name":"Animation"},{"id":35,"name":"Comedy"},{"id":80,"name":"Crime"},{"id":99,"name":"Documentary"},{"id":18,"name":"Drama"},{"id":10751,"name":"Family"},{"id":14,"name":"Fantasy"},{"id":36,"name":"History"},{"id":27,"name":"Horror"},{"id":10402,"name":"Music"},{"id":9648,"name":"Mystery"},{"id":10749,"name":"Romance"},{"id":878,"name":"Science Fiction"},{"id":10770,"name":"TV Movie"},{"id":53,"name":"Thriller"},{"id":10752,"name":"War"},{"id":37,"name":"Western"}];
			var names = [];
			var assigns = genre_ids.split(',');
			for ( i in assigns) {
				if ( parseInt(assigns[i]) ) {
					for ( g in genres ) {
						if ( assigns[i] == genres[g]['id'] ) {
							names.push(genres[g]['name']);
						}
					}
				}
			}			
			return names.join(', ');

		}
	})

};
//
// genre ids
// {"genres":[{"id":28,"name":"Action"},{"id":12,"name":"Adventure"},{"id":16,"name":"Animation"},{"id":35,"name":"Comedy"},{"id":80,"name":"Crime"},{"id":99,"name":"Documentary"},{"id":18,"name":"Drama"},{"id":10751,"name":"Family"},{"id":14,"name":"Fantasy"},{"id":36,"name":"History"},{"id":27,"name":"Horror"},{"id":10402,"name":"Music"},{"id":9648,"name":"Mystery"},{"id":10749,"name":"Romance"},{"id":878,"name":"Science Fiction"},{"id":10770,"name":"TV Movie"},{"id":53,"name":"Thriller"},{"id":10752,"name":"War"},{"id":37,"name":"Western"}]}
exports.buildsql_where = function(req, res) {
	attr={
		"where":['title IS NOT NULL', 'type IN ("Movies", "TV")', 't.overview IS NOT NULL', 't.vote_count > 10'], 
		"order by":[{'key':'lastupdated', 'dir':'desc'}], 
	};
	var wherecols = ['type'];

	for (const key in req.query) {
		v = req.query[key]; 
		if ( typeof v == 'string' ) v=[v];

		if (wherecols.includes(key) ) attr['where'].push(key + " IN (" + res.app.mysql.escape(v) + ")");
	}
	if ( req.query['search'] != undefined ) {
		var q = req.query['search']['value'].replace("'", "");
		attr['where'].push("title LIKE '%" + q + "%'");
	}

	var where = attr['where'].join(' AND ');
	return where;
}
exports.buildsql = function(req, res) {
	
	var where = exports.buildsql_where(req, res); 
	var limit = 100; 
	var offset = 0; 
	var sort={'key':'lastupdate', 'dir':'desc'};
	var sortcols=['vote_average', 'lastupdate', 'downloads'];

	if ( sortcols.includes(req.query['sort']) ) {
		sort.key=req.query['sort'];
	}
	if ( parseInt(req.query['length']) && req.query['length'] < 1000 ) {
		limit = req.query['length'];
	}
	if ( parseInt(req.query['start']) ) {
		offset = parseInt(req.query['start']);
	}
	var _sql = `
		SELECT 
			b.type,
			b.year,
			b.title,
			b.lastupdate,
			b.datecreated,
			b.downloads,
			t.genre_ids,
			t.overview, 
			t.popularity,
			t.poster_path,
			t.release_date,
			t.vote_average, 
			t.vote_count
		FROM
			browse b
			LEFT JOIN tmdb t USING (title, year)
		WHERE 
			`+where+`
		GROUP BY
			b.title, b.year, b.type
		ORDER BY 
			 `+sort.key+` DESC
		LIMIT ` + limit + ` OFFSET ` + offset + `
		`;
		console.log(_sql);

/*
	var _sql=`
		SELECT 
			count(*) AS total_files, 
			p.title AS title, 
			IF(p.year=0, NULL, p.year) AS year, 
			SUBSTRING_INDEX( GROUP_CONCAT(p.type), ',', 1) AS type, 
			MAX(p.lastupdate) AS lastupdate,
			SUM(p.downloads) AS downloads,
			t.genre_ids,
			t.overview, 
			t.poster_path,
			t.vote_average, 
			t.vote_count
		FROM
			packs p
			LEFT JOIN tmdb t USING (title, year)
		WHERE 
			`+where+`
		GROUP BY
			p.title, p.year
		ORDER BY 
			 `+sort.key+` DESC
		LIMIT ` + limit + ` OFFSET ` + offset + `
		`;
*/		
		console.log(_sql);
	return _sql;
};
exports.buildsql_count = function(req, res) {
	var where = exports.buildsql_where(req, res); 
	var _sql=`
		SELECT 
			COUNT(DISTINCT p.title) AS recordsFiltered,
			( SELECT count(DISTINCT title) FROM packs ) AS recordsTotal
 		FROM
			packs p
			LEFT JOIN tmdb t 
				USING ( title, year )
		WHERE 
			`+where+`
		`;
//		console.log(_sql);
	return _sql;

}

exports.load = function(req, res, next) {
	var _sql=exports.buildsql(req, res);
	var _sqlcount=exports.buildsql_count(req, res);
	
	//Fetch counts
	req.app.db.query(_sqlcount, function (error, results, fields) {
		if (error) {
			console.log(error);
			throw error;
		}
		req.app.locals.meta = results;
		req.app.db.query(_sql, function (error, results, fields) {
			if (error) {
				console.log(error);
				throw error;
			}
			console.log(results[0]);
			req.app.locals.titles = results;
			next();
		});

	});		
};
