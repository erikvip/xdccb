var createError = require('http-errors');
var express = require('express');
var path = require('path');
var session = require('express-session');
var compression = require('compression');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

require('dotenv').config()
//console.log(process.env)




const environment = process.env.NODE_ENV || 'development';

//console.log(environment);

if ( environment == 'development' ) {
	var livereload = require("livereload");
	var connectLiveReload = require("connect-livereload");
	//Reload on file mods
	const liveReloadServer = livereload.createServer();
	liveReloadServer.server.once("connection", () => {
	  setTimeout(() => {
	    liveReloadServer.refresh("/");
	  }, 100);
	});
}


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var packs = require('./packs');
var browse = require('./browse');


var app = express();
app.use(compression());
if ( environment == 'development' ) {
	//app.use(connectLiveReload());
}

app.use(session({secret: "Goddamnit it's serious now...she fell in love with me...eye contact, man"}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.mysql      = require('mysql');
app.db = app.mysql.createConnection({
  host     : 'localhost',
  user     : 'xdccb',
  password : 'xdccb',
  database : 'xdccb'
});
app.db.connect();

var Users = [{id:process.env.xdccb_admin_user, password: process.env.xdccb_admin_password }];
/*
app.use(function(req, res, next) {
	if ( req.app.get('env') != 'development' ) {
		//In production, the 'Authorization' HTTP header *must* be present,
		//check for it, and extract the username
		const auth = req.get('Authorization'); 
		if ( auth == undefined ) {
			console.log('No Authorization HTTP header in Production Enviorment. Quitting');
			process.exit(1);
		}
		//Extract username
		const username = auth.substr(auth.indexOf('username="')).split('"')[1];
		console.log("Username: " + username);
		app.locals.username=username;
		
	}
	next();	
});
*/

function checkSignIn(req, res, next){
   if(req.session.user){
      next();     //If session exists, proceed to page
   } else {
      //var err = new Error("Not logged in!");
      console.log("Not logged in");
      res.redirect('/login');
      console.log(req.session.user);
      next(err);  //Error, trying to access unauthorized page!
   }
}
//app.get('/protected_page', checkSignIn, function(req, res){
 //  res.render('protected_page', {id: req.session.user.id})
//});

app.get('/login', function(req, res){
   res.render('login', {
   	title: 'Login', 
   	message: 'Login to continue'
   });
});

app.post('/login', function(req, res){
   console.log(Users);
   if(!req.body.id || !req.body.password){
      res.render('login', {'title':'Login', message: "Please enter both id and password"});
   } else {
      Users.filter(function(user){
         if(user.id === req.body.id && user.password === req.body.password){
            req.session.user = user;
            res.redirect('/');
         }
      });
      res.render('login', {'title': 'login', message: "Invalid credentials!"});
   }
});


app.use('/', checkSignIn, indexRouter);
//app.all('/packs', packs.load);

//app.set('jsonp callback name', 'cb')
app.get('/packs/json', checkSignIn, function(req, res, next){
	packs.json(req, res, function() {
		var data = {
			draw : req.query['draw'],
			recordsTotal: app.locals.meta[0].recordsTotal,
			recordsFiltered: app.locals.meta[0].recordsFiltered,
			data: app.locals.packs
		};
		res.jsonp(data);
	});

});
app.get('/packs', checkSignIn, function(req, res, next){
	packs.load(req, res, function() {
		packs.html(req, res);
	});
	
});

app.get('/browse', checkSignIn, function(req, res, next){
	browse.load(req, res, function() {
		browse.html(req, res);
		next();
	});
});

//app.get('/packs/recent', packs.recent);


// catch 404 and forward to error handler
//app.use(function(req, res, next) {
//  next(createError(404));
//});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  console.log(err.message);
  console.log(res.locals.error);

  // render the error page
  res.status(err.status || 500);
  
  res.render('error', {title: 'Error'});
});

module.exports = app;
