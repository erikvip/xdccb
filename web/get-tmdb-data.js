if (process.stdout._handle) process.stdout._handle.setBlocking(true)
const express = require('express');
const request = require('request');
require('dotenv').config();

const TMDB_API_KEY=process.env.TMDB_API_KEY;

//const app = express();

const mysql      = require('mysql');

var titles = [];

const db = mysql.createConnection({
  host     : 'localhost',
  user     : 'xdccb',
  password : 'xdccb',
  database : 'xdccb'
});
db.connect();

const LIMIT = 20;


//db.query('SELECT title, year, type FROM packs p LEFT JOIN tmdb_refs r USING (title, year) WHERE r.title IS NULL AND p.year != 0 AND p.year IS NOT NULL AND p.type="Movies" GROUP BY p.title, p.year LIMIT ' + LIMIT, function(err, rows, fields) {
	db.query('SELECT title, year, type, GROUP_CONCAT(filename) AS files FROM packs p LEFT JOIN tmdb_refs r USING (title, year) WHERE r.title IS NULL AND p.year != 0 AND p.year IS NOT NULL AND p.type IN ("TV", "Movies") GROUP BY p.title, p.year, p.type ORDER BY RAND() LIMIT ' + LIMIT, function(err, rows, fields) {
	var c = 0; 
	var cp = 0; 
	if (err) {
		console.log(err);
		process.exit(1);
	} else {
		for (i in rows) {
			c++;
			var d = rows[i];
			let title = rows[i]['title'];
			let year = rows[i]['year'];
			let type = d['type'];
			var url;
			if (d['type'] == 'Movies') {
				url='https://api.themoviedb.org/3/search/movie?api_key='+TMDB_API_KEY+'&query='+encodeURIComponent(title)+'&year='+year;
			} else if (d['type'] == 'TV' ) {
				url='https://api.themoviedb.org/3/search/tv?api_key='+TMDB_API_KEY+'&query='+encodeURIComponent(title);
			} else {
				console.log("Unknown type: " + d);
				process.exit(1);
			}
			//console.log(d);
			//console.log(url);
			if ( year > 0 ) 
				url+='&first_air_date_year='+year;
			request(url, function(error, response, body) {
				if(error){
					console.log(error);
					process.exit()
				}else{
					let data = JSON.parse(body);
					//console.log(data)
					let r = data.results[0]; 
					//console.log(r);process.exit(1);
					if ( r != undefined ) {
						var id = r.id;
						if (type=="TV") {
							r.title = r.name;
							r.original_title = r.name;
							r.year = year;
							adult = 0; 
						}
						var _sql=`
							REPLACE INTO tmdb
								( 
									title, 
									year,
									tmdbid, 
									adult, 
									backdrop_path, 
									genre_ids, 
									original_language, 
									original_title, 
									overview, 
									popularity, 
									poster_path, 
									release_date, 
									vote_average, 
									vote_count
								)
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
						`;
						db.query(_sql, [
							r.title, 
							year, 
							r.id,
							r.adult, 
							r.backdrop_path,
							r.genre_ids.join(',') + ',', 
							r.original_language,
							r.original_title,
							r.overview,
							r.popularity,
							r.poster_path,
							r.release_date,
							r.vote_average,
							r.vote_count
						], function(err, rows, fields) {
							if(error){
								console.log(error);
								process.exit()
							}
						});
						//let what=(type=="TV") ? "TVShow" : "Movie ";
						var info="https://www.themoviedb.org/"; 

						info+=(type=="TV") ? 'tv/' : 'movie/';
						info+=id;

						console.log('Hits:' + data.results.length + info + ' ' + title + ' [' + year + '] ' + r.title );
					} else {
						console.log('No results for ' + title);
						var id = 0;
					}

					var _sql="REPLACE INTO tmdb_refs (title, year, tmdbid) VALUES(?,?,?)"; 
					db.query(_sql, [title, year, id], function() {
						if(error){
							console.log(error);
							process.exit()
						}else{
							cp++;
							if ( c == cp ) {
								//console.log("----- Success: " + c + "\t Failures: " + cp )
								process.exit();
							}
						}										
					});
				} 
			});								
		} // end for i in results
	} // end if error
});

