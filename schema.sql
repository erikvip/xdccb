CREATE TABLE `browse` (
  `type` varchar(16) NOT NULL,
  `year` int(11) NOT NULL DEFAULT '0',
  `title` varchar(512) NOT NULL,
  `lastupdate` datetime DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `downloads` int(11) DEFAULT '0',
  PRIMARY KEY (`type`,`year`,`title`)
);
CREATE TABLE `packs` (
  `network` varchar(32) NOT NULL,
  `channel` varchar(32) NOT NULL,
  `nickname` varchar(32) NOT NULL,
  `packno` int NOT NULL,
  `downloads` int NOT NULL DEFAULT '0',
  `size` varchar(16) NOT NULL,
  `type` varchar(16) DEFAULT NULL,
  `filename` varchar(1024) NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `year` int DEFAULT NULL,
  `itemid` varchar(32) DEFAULT NULL,
  `uri` varchar(2048) NOT NULL,
  `url` varchar(2048) NOT NULL,
  `lastannounced` datetime DEFAULT CURRENT_TIMESTAMP,
  `lastupdate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`network`,`channel`,`nickname`,`packno`),
  UNIQUE KEY `uri` (`uri`),
  UNIQUE KEY `url` (`url`)
);

CREATE TABLE `msg` (
  `msgid` int(11) NOT NULL AUTO_INCREMENT,
  `msgtype` varchar(255) NOT NULL DEFAULT 'xdccget',
  `msgdata` text,
  `processed` int(11) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`msgid`)
);
CREATE TABLE `xdccqueue` (
  `xdcc_url` varchar(1024) NOT NULL,
  `status` varchar(32) NOT NULL DEFAULT 'pending',
  `finished` int NOT NULL DEFAULT '0',
  `errors` int NOT NULL DEFAULT '0',
  `attempts` int NOT NULL DEFAULT '0',
  `total_bytes` bigint DEFAULT NULL,
  `recv_bytes` bigint DEFAULT '0',
  `progress` int DEFAULT '0',
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  `date_started` datetime DEFAULT NULL,
  `date_finished` datetime DEFAULT NULL,
  `servertag` varchar(32) DEFAULT NULL,
  `nick` varchar(32) DEFAULT NULL,
  `addr` varchar(32) DEFAULT NULL,
  `port` int DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`xdcc_url`)
);


CREATE TABLE `eventlog` (
  `eventid` int(11) NOT NULL AUTO_INCREMENT,
  `eventtype` varchar(255) NOT NULL DEFAULT 'info',
  `eventdata` text,
  `notified` int(11) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`eventid`)
);

CREATE TABLE `tmdb` (
  `title` varchar(1024) NOT NULL,
  `year` int(11) NOT NULL,
  `tmdbid` int(11) NOT NULL,
  `adult` int(11) DEFAULT '0',
  `backdrop_path` varchar(1024) DEFAULT NULL,
  `genre_ids` varchar(1024) DEFAULT NULL,
  `original_language` varchar(32) DEFAULT NULL,
  `original_title` varchar(1024) DEFAULT NULL,
  `overview` text,
  `popularity` decimal(8,3) DEFAULT NULL,
  `poster_path` varchar(1024) DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `vote_average` decimal(8,3) DEFAULT NULL,
  `vote_count` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`title`,`year`),
  UNIQUE KEY `tmdbid` (`tmdbid`)
);

CREATE TABLE `tmdb_refs` (
  `title` varchar(2048) NOT NULL,
  `year` int(11) NOT NULL,
  `tmdbid` int(11) NOT NULL,
  PRIMARY KEY (`title`,`year`,`tmdbid`)
);
