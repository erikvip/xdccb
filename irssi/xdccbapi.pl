=pod

=head1 NAME

xdccbapi.pl

=head1 DESCRIPTION

A simple API to control irssi XDCC requests

=head1 INSTALLATION

Copy into your F<~/.irssi/scripts/> directory and load with
C</SCRIPT LOAD F<filename>>.

=head1 SETUP

See the xdccb docs

=head1 USAGE

Symlink to ~/.irssi/scripts/autorun/xdccbapi.pl

=head1 AUTHORS

Copyright E<copy> 2021 erikvip C<E<lt>github.com/erikvipE<gt>>

=head1 LICENCE

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.



=cut

use strict;
use warnings;

use Irssi;
use Irssi::Irc;
use Irssi::TextUI;
use Data::Dumper;
use IO::Socket::UNIX;

use POSIX;
use Time::HiRes qw/sleep/;
use DBI;

my $forked = 0;

our $VERSION = '0.74';
our %IRSSI = (
							authors     => 'erikvip',
							contact     => 'github.com/erikvip',
							name        => 'xdccbapi',
							description => 'Irssi API bridge for my XDCC manager',
							license     => 'MIT',
							updated     => '2023-12-10'
						 );

my $last_xdccbqueue_update=0;
my $dsn = "DBI:mysql:xdccb";
my $username = "xdccb";
my $password = 'xdccb';

# connect to MySQL database
my %attr = ( 
	PrintError=>0,  # turn off error reporting via warn()
	RaiseError=>1,  # turn on error reporting via die()           
	mysql_auto_reconnect=>1   
);   
my $dbh  = DBI->connect($dsn,$username,$password, \%attr);

sub _error {
		my ($msg) = @_;
		my $win = Irssi::active_win();
		$win->print($msg, Irssi::MSGLEVEL_CLIENTERROR);
}

sub _log {
		my ($msg) = @_;
		my $win = Irssi::active_win();
		$win->print($msg, Irssi::MSGLEVEL_CLIENTCRAP);
}

sub _mysql_query {
	my $sql=shift;
	my $res=`echo '$sql' | mysql --user=xdccb --password=xdccb --database=xdccb -N 2>/dev/null`;
	return $res;
	
}

sub _update_xdcc_queue {
	my $max      = Irssi::settings_get_int('xdcc_max_transfers');
	my $count     = dcc_getcount();
	if ( $count < $max ) {
		my $limit = $max - $count;
		my $sql='SELECT * FROM xdccqueue WHERE finished!=1 AND status="pending" ORDER BY date_added'; 
		my $res=_mysql_query $sql;
		foreach my $row ( split(/\n/, $res) ) {
			my ($xdcc_url, $status) = split(/\t/, $row);
			my ($_network, $_channel, $_nickname, $_packno, $_filename) = split(/\//, $xdcc_url);
			# Verify we are connected to requested _network
			if ( Irssi::server_find_chatnet("$_network") ) {
				# Now verify we've been connected for at least 2 minutes (some servers require this)
				if ( Irssi::server_find_chatnet("$_network")->{connected} == 1 && time() - Irssi::server_find_chatnet("$_network")->{real_connect_time} > 120 ) {
					_log "Requesting xdcc send of $xdcc_url\n";
					# Looks good...queue up this file...
					_log("_update_xdcc_queue: send_message: $_nickname 'XDCC SEND #$_packno'");
					Irssi::server_find_chatnet("$_network")->send_message("$_nickname","XDCC SEND #$_packno",1);
					Irssi::server_find_chatnet("$_network")->send_message("$_nickname","\x01XDCC SEND #$_packno\x01",1);

					my $sql="UPDATE xdccqueue SET status='requested' WHERE xdcc_url='$xdcc_url'";
					my $sth=$dbh->prepare($sql);
					$sth->execute();
				}
			}
		}
	}
}

sub dcc_getcount { 
		my @dccs  = Irssi::Irc::dccs();
		my $count = 0;
		foreach my $dcc (@dccs) { $count++ if $dcc->{type} eq "GET" || $dcc->{type} eq "SEND"; }
		return $count;
}


sub _process_msg_queue { 
		my $sql="SELECT * FROM msg WHERE processed=0 ORDER BY created ASC";
		my $res=_mysql_query($sql);
#    _log "Process message queue";
		foreach my $line ( split(/\n/, $res) ) {
			my ( $msgid, $msgtype, $msgdata, $msgprocessed, $msgdate ) = split(/\t/, $line);
			_log "$msgid $msgtype $msgdata $msgprocessed $msgdate";
			if ( $msgtype eq "xdccget" ) {
				#_enqueue_xdcc($msgdata); 
				$sql="INSERT INTO xdccqueue (xdcc_url) VALUES ( (SELECT msgdata FROM msg WHERE msgid='$msgid') )";
				_mysql_query($sql);

				$sql="UPDATE msg SET processed=1 WHERE msgid='$msgid'";
				_mysql_query($sql);
			}
		}
}

sub process_queues {
		#_log "Process queues";
		_process_msg_queue
		_update_xdcc_queue

		my @dccs  = Irssi::Irc::dccs();

		foreach my $dcc (@dccs) { 
			if ( $dcc->{type} eq "GET" || $dcc->{type} eq "SEND" ) {
					_xdcc_update_progress($dcc, '');
			}  
		}      
}

sub sig_dcc_connected {
		_log("sig_dcc_connected");
		my($dcc) = @_;
		_xdcc_update_progress($dcc, "connected");
}
sub sig_dcc_transfer_update {
#    _log("sig_dcc_transfer_update");
		my($dcc) = @_;
		
		_xdcc_update_progress($dcc, "transferring");
}

sub _xdcc_update_progress {
 #   _log("_xdcc_update_progress");
		my ($dcc, $_status) = @_;

#    my $time=time();
		# Only update thbe database every 30 seconds
 #   if ( $last_xdccbqueue_update - $time < 30 ) {
	#      return;
 #   }

		my $_finished=0;
		my $_errors=0; 
		my $_datefinished="NULL";

		if ( $_status eq "failed" ) {
			$_errors=1; 
		}
		if ( $_status eq "complete" ) {
			$_finished=1;
			$_datefinished="NOW()";
		}

		my $_file=$dcc->{arg};
		# Scrub unsafe characters from filename (including UTF8)
		$_file=~tr/[\_A-Z\a-z0-9 :#_\-\.\n\[\]\|]//cd;

		my $_nick=$dcc->{nick};
		my $_server=$dcc->{servertag};
		my $_size=$dcc->{size};
		my $_transfd=$dcc->{transfd};
		my $_starttime=$dcc->{starttime};
		
		my $_progress=0; 
		if ( $_transfd > 0 ) {
			$_progress=($_transfd / $_size * 100);
		}
		if ( $_progress eq "" ) {
			$_progress=0; 
		}
		my $sql="UPDATE xdccqueue SET "; 
		if (! $_status eq "") { $sql.=" status='$_status',"; }



		$sql .= " 
							finished='$_finished', 
							errors='$_errors',
							total_bytes='$_size', 
							recv_bytes='$_transfd', 
							progress='$_progress', 
							date_started=FROM_UNIXTIME('$_starttime'),
							date_finished=$_datefinished,   -- No quotes here
							servertag='$dcc->{servertag}',
							nick='$dcc->{nick}',
							addr='$dcc->{addr}',
							port='$dcc->{port}',
							description=''
						WHERE
							xdcc_url LIKE '${_server}/%/$_nick/%/$_file'
		";
#    _log($sql);
		my $sth=$dbh->prepare($sql);
		$sth->execute();
}

sub sig_dcc_closed {
	#  _log("sig dcc closed");
		my($dcc) = @_;
		my($dir,$file);

		return unless $dcc->{type} eq 'GET';
		return unless -f $dcc->{file};

		($dir,$file) = $dcc->{file} =~ m,(.*)/(.*),;
		$dir .= "/done";

		if ($dcc->{transfd} < $dcc->{size}) {
				_xdcc_update_progress($dcc, "failed");
			return;
		}
		
		_xdcc_update_progress($dcc, "complete");

		# Scrub unsafe characters from filename (including UTF8)
		$file=~tr/[\_A-Z\a-z0-9 :#_\-\.\n\[\]\|]//cd;

		_log "mkdir $dir";
		_log "move $dcc->{file} to $dir/$file";   
		mkdir $dir, 0755 unless -d $dir;
		rename $dcc->{file}, "$dir/$file";
		printf('%%gDCC moved %%_%s%%_ to %%_%s%%_%%n', $file, $dir);

}

## Remove any non-ascii or invalid characters from filename
## interception made by registering signal as first + Irssi::signal_continue()
sub event_ctcp_dccsend {
    my ($server, $args, $nick, $addr, $target) = @_;

    _log("event_ctcp_dccsend: $server, $args, $nick, $addr, $target");

    # split incomming send request args into filename (either before first space or
    #  quoted), and the rest (IP, port, +optionally filesize)
    my ($filename, $rest) = $args =~ /((?:".*")|\S*)\s+(.*)/;

    # remember file name for informing sake
    my $oldname = $filename;
    $filename=~tr/[\_A-Z\a-z0-9 :#_\-\.\n\[\]\|]//cd;

    _log("event_ctcp_dccsend: oldname: $oldname filename: $filename");
    if ($filename ne $oldname ) {
        # some info for user
        _log("event_ctcp_dccsend: correcting filename to $filename");
		Irssi::print('DCC SEND request from '.$nick.': renamed bad filename '.$oldname.' to '.$filename);
        # propagate signal; Irssi will proceed the request with altered arguments ($args) 
        Irssi::signal_continue($server, $args, $nick, $addr, $target);  
    }
}

sub _msg_warn {
    my ($msg) = @_;
    my $win = Irssi::active_win();
    $win->print( $msg, Irssi::MSGLEVEL_CLIENTERROR );
    return;
}

sub _msg_status {
    my ($msg) = @_;
    my $win = Irssi::window_find_name('(status)');
    $win->print( $msg, Irssi::MSGLEVEL_CLIENTCRAP );
    return;
}

#my $sockpath='/tmp/xdccb.sock';
#unlink($sockpath);
#my $sock = IO::Socket::UNIX->new(
#	Type => SOCK_STREAM,
#	Local => $sockpath, 
#	Listen => 1
#)  or die "Cannot create socket - $@\n";
#$sock->blocking(0);

#sub _sock_connreq {
#	process_queues;
#	_msg_status "xdccb: _sock_connreq";
#	my $z = $sock->accept or return;
#	$z->blocking(0);
#	my $data; 
#	if ( $z->read(my $buf, 1024) ) {
#		$data .= $buf;
#	}
#	_msg_status "xdccb: sock read: $data";
#	$z->close if $z; 
#	$z->shutdown(SHUT_WR);
#}

#sub _sock_close {
#	_msg_status "_sock_close";
#	Irssi::input_remove($sock->fileno);
#	$sock->shutdown(SHUT_RDWR);
#	$sock->close();
#	unlink($sockpath);
#}
#sub UNLOAD {
#	_sock_close()

#}
#sub cmd_start {
#	_msg_status "XDCCB Start. UNIX Socket at: $sock->hostpath";
#	Irssi::input_add( $sock->fileno, Irssi::INPUT_READ, '_sock_connreq', undef);
#}

Irssi::settings_add_int($IRSSI{'name'}, 'xdcc_max_transfers', 5);
Irssi::signal_add('dcc connected', 'sig_dcc_connected');

# register signal of incoming ctcp 'DCC SEND', before anything else
Irssi::signal_add_first('ctcp msg dcc send', 'event_ctcp_dccsend');

# This is too intensive, instead call it manually 
#Irssi::signal_add('dcc transfer update', 'sig_dcc_transfer_update');
Irssi::signal_add_last('dcc closed', 'sig_dcc_closed');
Irssi::command_bind("xdccbq", \&process_queues);
Irssi::timeout_add(15000, \&process_queues, undef);
# Timeout here to print our message after the loading notice
#Irssi::timeout_add_once 200, \&cmd_start, undef;
