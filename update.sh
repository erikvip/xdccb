#!/bin/bash

declare -A xdccb=( [CONFIG_FILE]="${HOME}/.xdccb.conf" [DEBUG]=false )

BASEDIR=$(dirname $0);

process() {
	debug "Outputing to CSV list at ${xdccb[CSV_FILE]}"
	for i in ${xdccb[CHANNELS]}; do
		debug "Processing ${i//#/\\#}"
		_in=(${i//;/ })
		_logfile="${_in[0]}"
		_network="${_in[1]}"
		_channel="${_in[2]}"
		_snapshot=$(date "+${xdccb[IRC_LOG_DIR]}/xdccb/snapshot-${_network}-${_channel}-%Y%m%d%H%M%S.log");
		#flock "${_logfile}" -c "cp '${_logfile}' '${_snapshot}'; truncate -s 0 '${_logfile}'"
		cp "${_logfile}" "${_snapshot}"
		truncate -s 0 "${_logfile}"
		${BASEDIR}/parse.sh "${_snapshot}" "${_in[1]}" "${_in[2]}" >> "${xdccb[CSV_FILE]}"
		gzip "${_snapshot}"
		tar rf "${xdccb[TAR_LOG_FILE]}" "${_snapshot}.gz"
		rm "${_snapshot}.gz"
	done

	debug "Loading data into MySQL"

	_sql=$(cat << END_QUERY

		CREATE TEMPORARY TABLE import LIKE packs;

		LOAD DATA LOCAL INFILE '${xdccb[CSV_FILE]}' INTO TABLE import FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY "'" LINES TERMINATED BY '\n';

		-- Packs that have not been updated, just update announcement date & total downloads
		INSERT INTO packs ( 
			SELECT 
				i.network, 
				i.channel, 
				i.nickname, 
				i.packno, 
				i.downloads, 
				i.size, 
				i.type, 
				i.filename, 
				i.title, 
				i.year, 
				i.itemid, 
				i.uri, 
				i.url, 
				NOW(), 
				p.lastupdate 
			FROM import i 
				LEFT JOIN packs p USING (uri, url) 
			WHERE 
				i.uri=p.uri 
				AND i.url=p.url 
				AND p.url IS NOT NULL
		) ON DUPLICATE KEY UPDATE downloads=i.downloads,lastannounced=NOW();

		-- Updated packs with different filenames
		REPLACE INTO packs ( SELECT i.network, i.channel, i.nickname, i.packno, i.downloads, i.size, i.type, i.filename, i.title, i.year, i.itemid, i.uri, i.url, NOW(), NOW() FROM import i LEFT JOIN packs p USING (uri) WHERE i.uri=p.uri AND i.filename!=p.filename and p.filename IS NOT NULL);

		-- New packs / id's
		INSERT INTO packs ( SELECT i.network, i.channel, i.nickname, i.packno, i.downloads, i.size, i.type, i.filename, i.title, i.year, i.itemid, i.uri, i.url, NOW(), NOW() FROM import i WHERE i.uri NOT IN ( SELECT uri FROM packs) ); 

		-- # remove any packs that haven't been reannoucned...
		DELETE FROM packs WHERE lastannounced < NOW() - INTERVAL 2 DAY;

		REPLACE  
		 INTO browse
		SELECT 
			p.type,
			IFNULL(p.year, 0),
			p.title AS title, 
			MAX(p.lastupdate) AS lastupdate,
			MIN(p.lastupdate) AS datecreated,
			SUM(p.downloads) AS downloads
		FROM
			packs p
				LEFT JOIN tmdb t USING (title, year)
		WHERE 
			p.title IS NOT NULL
			AND p.type IN ('TV', 'Movies')
		GROUP BY
			p.title, p.year, p.type


END_QUERY
);

	mysql_query "SELECT count(*) FROM packs;"; 
	mysql_query "${_sql}";
	mysql_query "SELECT count(*) FROM packs;"; 

	#mysql_query "SELECT count(*) FROM packs; LOAD DATA LOCAL INFILE '${xdccb[CSV_FILE]}' INTO TABLE packs FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY \"'\" LINES TERMINATED BY '\n'; SELECT COUNT(*) FROM packs;"
	#mysql_query "UPDATE packs SET lastupdate=NOW() WHERE lastupdate IS NULL"

	gzip "${xdccb[CSV_FILE]}"
	tar rf "${xdccb[TAR_LOG_FILE]}" "${xdccb[CSV_FILE]}.gz"
	rm "${xdccb[CSV_FILE]}.gz"
}

addchannel() {
	local _logfile="$1"
	local _network="$2"
	local _channel="$3"
	local _url="${_logfile};${_network};${_channel}";
	debug "Adding channel $_url"
	xdccb[CHANNELS]="${xdccb[CHANNELS]}${_url} ";
}

mysql_query() {
	local _user="";
	local _pass="";
	[[ ${xdccb[MYSQL_USER]} != "" ]] && _user="--user=${xdccb[MYSQL_USER]}";
	[[ ${xdccb[MYSQL_PASS]} != "" ]] && _pass="--password=${xdccb[MYSQL_PASS]}";	

	_msg=`echo "$*" | mysql $_user $_pass --host="${xdccb[MYSQL_HOST]}" --database="${xdccb[MYSQL_DB]}" 2>&1`
	_ret=$?;
	[[ "$_ret" -ne 0 ]] && error "MySQL error: $_msg";	
	debug "MySQL return status: $_ret. Response: $_msg"
}

error() {
	echo $*
	exit 1
}
debug() {
	if [[ "${xdccb[DEBUG]}" == true ]]; then
		echo "DEBUG: $*"
	fi
}

setup() {
	debug "Sanity checking"
	# Sanity check
	[ ! -e ${xdccb[CONFIG_FILE]} ] && error "Could not find config file at ${xdccb[CONFIG_FILE]} ";
	source ${xdccb[CONFIG_FILE]};
 	mysql_query "SELECT NOW();"
 	mkdir -p "${xdccb[IRC_LOG_DIR]}/xdccb" || error "Could not create archive directory";
 	xdccb[TAR_LOG_FILE]=$(date "+${xdccb[IRC_LOG_DIR]}/xdccb/logfiles-%Y%m%d.tar");
 	xdccb[CSV_FILE]=$(date "+${xdccb[IRC_LOG_DIR]}/xdccb/listing-%Y%m%d%H%M%S.csv");
 	debug "Checking for tar archive log at ${xdccb[TAR_LOG_FILE]}";
 	if [[ ! -e "${xdccb[TAR_LOG_FILE]}" ]]; then
 		debug "Creating empty tar archive at ${xdccb[TAR_LOG_FILE]}";
 		tar cf "${xdccb[TAR_LOG_FILE]}" -T /dev/null || error "Could not create tar archive"
 	fi

 	process

}
getcmdline() {
	while getopts "c:d" opt; do
		case "$opt" in
			c)
				xdccb[CONFIG_FILE]="$OPTARG"
				break
				;;
			d)
				xdccb[DEBUG]=true;
				break
				;;
			*)
				error "Unrecognized option";
				;;
		esac;
	done
}


main() {
	getcmdline $*
	setup
}
main $*
